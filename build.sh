#!/bin/bash

if [ ! -d ".git" ]
then
    git clone https://aur.archlinux.org/rstudio-desktop-git.git .
fi

makepkg --syncdeps --noconfirm --force

cp *.pkg.tar.xz /repo/
