IMG_NAME = rstudio-buildroot
WD_PATH = /build

image: Dockerfile
	docker build -t $(IMG_NAME) .

package:
	docker run --rm -ti -v ${PWD}/build:/build -v ${PWD}/repo:/repo $(IMG_NAME)

.PHONY: config build backup image
