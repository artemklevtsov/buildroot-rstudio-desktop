FROM pritunl/archlinux:latest

RUN pacman -Syuq --noconfirm --needed git base-devel libgl &&\
    useradd -u 1000 -d /build build-user &&\
    echo 'build-user ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers &&\
    mkdir -p /build &&\
    chown build-user:build-user /build &&\
    mkdir -p /repo &&\
    chown build-user:build-user /repo &&\
    rm -rf /var/cache/pacman/pkg/*

ADD build.sh /usr/local/bin/build-rstudio

VOLUME ["/build", "/repo"]

WORKDIR /build

USER build-user

ENTRYPOINT ["/usr/local/bin/build-rstudio"]
